﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHP : MonoBehaviour
{
    public int health = 100;
    //sets up hp and amount of damage an enemy /player takes per hit. and if he dies he dies !
    public void TakeDamage (int damage)
    {
        health -= damage;
        
        if(health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        Destroy(gameObject);
    }
}
