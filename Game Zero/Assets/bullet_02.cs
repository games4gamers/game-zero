﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet_02 : Projectile
{
    public GameObject smallerBullets;

    private void OnDestroy()
    {
        for (int i = 0; i < 3; i++)
        {
            Instantiate(smallerBullets, transform.position, transform.rotation);
        }
    }


}
